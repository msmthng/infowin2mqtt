
# Infowin2Mqtt

## Purpose

infowin2mqtt is a python based script with is using the InfoWin API to read out data and setting data on the Windhager device connected to InfoWin.

## Using Docker Container

docker.io/msmthng/infowin2mqtt


#!/bin/bash

IMAGE=docker.io/msmthng/infowin2mqtt:0.0.3d
NAME=infowin2mqtt-production

docker pull $IMAGE

docker stop $NAME
docker rm $NAME

docker run -d \
--name $NAME \
--restart=unless-stopped \
-e "MES_HOST=INFOWINIP" \
-e "MES_PASSWORD=XXXXXXXX" \
-e "LOGLEVEL=INFO" \
-e "MQTT_HOST=MQTTSERVERIP" \
-e "MQTT_PORT=1883" \
-e "MES_USER=USER" \
$IMAGE
