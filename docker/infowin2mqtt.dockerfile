#FROM python:3.8.2-alpine3.11
FROM python:2.7.16-alpine


RUN pip install paho-mqtt
RUN pip install requests
RUN apk add bash



COPY ./docker/run.sh /
ADD ./source/infowin2mqtt.py /

ENV MES_HOST=127.0.0.1
ENV MES_USER=USER
ENV MES_PASSWORD=password

ENV MQTT_HOST=127.0.0.1
ENV MQTT_PORT=1883
ENV MQQT_ROOTTOPIC=windhager

ENV LOGLEVEL INFO

#ENTRYPOINT [ "/bin/bash" ]

RUN chmod +x /run.sh

#ENTRYPOINT [ "./run.sh" ]

ENTRYPOINT ["python", "./infowin2mqtt.py"]

