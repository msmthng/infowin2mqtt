# coding=utf-8
import os
import time
import logging

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


loglevel = os.getenv('LOGLEVEL', 'INFO')

logger = logging.getLogger('infowin2mqtt')

logger.setLevel(loglevel)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

handler = logging.StreamHandler()
handler.setFormatter(formatter)
# add the handler to the logger
logger.addHandler(handler)


def getMqttKey(key, specialReplace = False):

    if specialReplace:
        return key.encode('ascii', 'replace')
    else:
        return key



import xml.etree.ElementTree as ET

import requests
from requests.auth import HTTPDigestAuth

import json

import paho.mqtt.client as mqtt


windhagerHost = os.environ.get('MES_HOST', '192.168.1.146')
rootTopic = os.environ.get('MQQT_ROOTTOPIC','windhager')

#import sys
#sys.getdefaultencoding()

logger.info('reading config from infowin endpoints')

logger.info('reading functions configurations')

r = requests.get('http://%s/res/xml/EbenenTexte_de.xml' % (windhagerHost))
layerTextXml = ET.fromstring(r.text)
functionsCfg = {}
for fcttyp in layerTextXml.findall('fcttyp'):
    #print(fcttyp.get('id'))
    layers = {}
    for layer in fcttyp.findall('ebene'):
        #print(layer.get('id'),layer.text)
        layers[str(layer.get('id'))]=layer.text
    functionsCfg[str(fcttyp.get('id'))]=layers
#print functions

logger.info('reading groupmember configurations')

r = requests.get('http://%s/res/xml/VarIdentTexte_de.xml' % (windhagerHost))
varIdentTexteXml = ET.fromstring(r.text)
groupMembers = {}
for groups in varIdentTexteXml.findall('gn'):
    #print(groups.get('id'))
    groupMembers[str(groups.get('id'))] = {}
    for member in groups.findall('mn'):
        #print(member.get('id'),member.text
        groupMembers[str(groups.get('id'))][str(member.get('id'))] = {'name':member.text}
#http://192.168.1.146/res/xml/AufzaehlTexte_de.xml

logger.info('reading enumerations configurations')

config_processed = False
while not config_processed:
    r = requests.get('http://%s/res/xml/AufzaehlTexte_de.xml' % (windhagerHost))
    try:
        listValuesXml = ET.fromstring(r.text)
        config_processed = True
    except xml.etree.ElementTree.ParseError:
        config_processed = False
enumerations = {}
for groups in listValuesXml.findall('gn'):
    #print(groups.get('id'))
    for member in groups.findall('mn'):
        #print(member.get('id'))
        #groupMembers[str(groups.get('id'))][str(member.get('id'))]['values']=[]
        groupMembers[str(groups.get('id'))][str(member.get('id'))]['values']={}
        for enum in member.findall('enum'):
            value = enum.get('id')
            name = enum.text
            #groupMembers[str(groups.get('id'))][str(member.get('id'))]['values'].append({'value':value,'name':name})
            groupMembers[str(groups.get('id'))][str(member.get('id'))]['values'][value] = name
            ##print(value, name)
            ##die
#json.dumps(groupMembers)
#print groupMembers
#die

logger.info('reading errorcodes configurations')

r = requests.get('http://%s/res/xml/ErrorTexte_de.xml' % (windhagerHost))
errorCodesXml = ET.fromstring(r.text)
errors = {}
for error in errorCodesXml.findall('error'):
    errors[error.get('code')] = error.get('text')
json.dumps(errors)
errors['-'] = 'OK'

errorlist = []
for errorkey in errors:
    errorlist.append({'value':errorkey,'name':errors[errorkey]})

#----------------------------------------------------
#read Values
#----------------------------------------------------

from requests.auth import HTTPDigestAuth
import os

mesUser = os.environ.get('MES_USER','USER')
mesPassword = os.environ.get('MES_PASSWORD')


httpProxy = os.environ.get('HTTP_PROXY', '')

if httpProxy:
    proxyDict = {
        "http": httpProxy
    }
else:
    proxyDict = None


configuredOIDs = {}
configuredMQTTs = {}

logger.info('reading device configurations')

r = requests.get('http://%s/api/1.0/lookup/1?count=-1&offset=0' % (windhagerHost), proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
if r.status_code == 200:
    j = json.loads(r.text)
else:
    logger.error(r.status_code)
    exit(1)

for node in j:
    device_processed = False
    while not device_processed:
        r = requests.get('http://%s/api/1.0/lookup/1/%s?count=-1&offset=0' % (windhagerHost, node.get('nodeId')), proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
        try:
            functions = json.loads(r.text)
            device_processed = True
        except ValueError:
            logger.warning('device processing status code: %d' % r.status_code)
            device_processed = False
    for function in functions.get('functions'):
        r = requests.get('http://%s/api/1.0/lookup/1/%s/%s?count=100&offset=0' % (windhagerHost, node.get('nodeId'), function.get('fctId')), proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
        fName = function.get('name')
        fctType = function.get('fctType')
        groupMembersRequest = json.loads(r.text)
        for groupMember in groupMembersRequest:
            json.dumps(groupMember)
            groupId = groupMember.get('id')
            url = 'http://%s/api/1.0/lookup/1/%s/%s/%s?count=100&offset=0' % (windhagerHost, node.get('nodeId'), function.get('fctId'), groupId)
            #print(url)
            r = requests.get(url, proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
            if r.status_code == 200:
                groupItems = json.loads(r.text)
                index = 0
                for item in groupItems:
                    #('itemDetails', {'httpRequest': 'http://192.168.1.146/api/1.0/lookup/1/20/0/98/1?count=100&offset=0', u'typeId': 13, u'stepId': 1, u'name': u'05-051',
                    # u'timestamp': u'2020-03-28 12:21:29', u'OID': u'/1/20/0/5/51/0', u'subtypeId': -1, u'maxValue': u'75.0', u'value': u'65.0', u'minValue': u'10.0', u'groupNr': 5,
                    #u'unit': u'\xb0C', u'unitId': 1, u'step': u'0.5', u'writeProt': False, u'memberNr': 51})
                    #('itemDetails', {u'message': u'Not Found', u'code': 404, u'reason': u"Record with filter:'subnet='1' AND nodeId='60' AND nvIndex='99'' not found!"})
                    url = 'http://%s/api/1.0/lookup/1/%s/%s/%s/%d?count=1&offset=0' % (windhagerHost, node.get('nodeId'), function.get('fctId'), groupId, index)


                    logger.debug(url)
                    item_processed = False
                    while not item_processed:
                        r = requests.get(url, proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
                        try:
                            details = json.loads(r.text)
                            item_processed = True
                        except ValueError:
                            logger.warning('item processing status code: %d' % r.status_code)
                            item_processed = False
                    if not 'message' in details and not 'code' in details:
                        details['httpRequest'] = url
                        #groupMembers[str(groups.get('id'))][str(member.get('id'))] = {'name':member.text}
                        try:
                            pathAddon =  '/' + groupMembers[str(details['groupNr'])][str(details['memberNr'])]['name']
                        except KeyError:
                            pathAddon = ''
                        try:
                            details['path'] = (fName + '/' + functionsCfg[str(fctType)][str(groupId)]) + pathAddon
                            details['mqttPath'] = (fName + '/' + functionsCfg[str(fctType)][str(groupId)]).replace(' ','_').replace("'",'').replace('/','') + pathAddon.replace(' ','_').replace("'",'')#.decode('utf-8').replace('\xdc','Ü')#.encode('UTF-8')
                            #if details['OID'] == '/1/20/0/7/1/0':
                            #    print(details)
                            #    exit(0)
                            details['myDescription'] = groupMembers[str(details['groupNr'])][str(details['memberNr'])]['name']
                        except KeyError:
                            details['myDescription'] = ''
                        try:
                            details['valuesList'] = groupMembers[str(details['groupNr'])][str(details['memberNr'])]['values']
                        except KeyError:
                            pass

                        try:
                            if 'valueList' in details:
                                details['valueType'] = 'enum'
                            elif 'value' in details:
                                if str(float(details['value'])) == details['value']:
                                    details['valueType'] = 'float'
                                elif str(int(details['value'])) == details['value']:
                                    details['valueType'] = 'int'
                                else:
                                    details['valueType'] = 'unknown'
                            else:
                                details['valueType'] = 'unknown'
                        except:
                            logger.warning('valuetype could not be calculated for item %s' % details['path'])
                            details['valueType'] = 'unknown'
                            pass

                        #print('itemDetails', details)
                        oid = '%s/%s/%s/%d' % (node.get('nodeId'), function.get('fctId'), groupId, index)


                        if 'nvName'in details:
                            details['mqttPath'] = details['mqttPath'] + '/' + details.get('nvName')
                            details['path'] = details['path'] + '/' + details.get('nvName')


                        mqttKey = getMqttKey('%s/set/%s/' % (rootTopic,details['mqttPath']))

                        details['url'] = url

                        configuredOIDs[oid] = details
                        configuredMQTTs[mqttKey] = details

                        logger.debug({'key': mqttKey, 'dict':configuredMQTTs[mqttKey]})

                        logger.debug(details)
                        logger.info('configured item %s', details['path'])
                    index = index + 1
            else:
                logger.warning(url)
                logger.warning(r.status_code)



logger.info('configured hardcoded items')



#auch via http call configurieren

addingOIDs = {

'ALARM':'1/60/0/2/0/0',
'KESSELLEISTUNG':'1/60/0/0/9/0',
'ABGASTEMPERATUR':'1/60/0/0/11/0',
'PHASE':'1/60/0/2/1/0',
'VERBRAUCH_GES':'1/60/0/23/103/0',
'VERBRAUCH_FUELLUNG':'1/60/0/23/100/0',  #wie fuellung setzen?
'T_REINIGUNG':'1/60/0/20/61/0',
'T_HAUPTREINIGUNG':'1/60/0/20/62/0',
'T_WARTUNG':'1/60/0/20/63/0',
'BRENNERSTARTS':'1/60/0/2/80/0',
'BETRIEBSSTUNDEN':'1/60/0/2/81/0',
'KESSEL_SOLL_TEMP':'1/60/0/1/7/0'

}

for item in addingOIDs:
    oid = addingOIDs[item]
    details = {
        'httpRequest': 'http://%s/api/1.0/lookup/%s?count=1&offset=0' % (windhagerHost, oid),
        'url': 'http://%s/api/1.0/lookup/%s?count=1&offset=0' % (windhagerHost, oid),
        'OID': '%s' % (addingOIDs[item]),
        'mqttPath':'STATUS/%s' % (item),
        'path':'STATUS/%s' % (item),
    }

    oid_processing = False
    while not oid_processing:
        try:
            r = requests.get(details['url'], proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))
            j = json.loads(r.text)
            oid_processing = True
        except ValueError:
            oid_processing = False

    try:
        details['valuesList'] = groupMembers[str(j['groupNr'])][str(j['memberNr'])]['values']
        details['valueType'] = 'enum'
    except KeyError:
        pass

    if item == 'ALARM':
        details['valueType'] = 'enum'
        #details['valuesList'] = errorlist
        details['valuesList'] = errors

    configuredOIDs[oid] = details
    mqttKey = getMqttKey('%s/set/%s/' % (rootTopic,details['mqttPath']))
    configuredMQTTs[mqttKey] = details

    logger.debug(details)
    logger.info('configured item %s', details['path'])

    #if item == 'ALARM':
    #    print(details)

#configuredOIDs[]



logger.info('%d items configured for reading out and handling on MTQQ' % (len(configuredOIDs)))

#----------------------------------------


scriptPath = os.path.dirname(os.path.realpath(__file__))


#with open('%s/items.json' % scriptPath) as json_file:
#    data = json.load(json_file)
#items = data['items']

#print(items)

mgttHost = os.environ.get('MQTT_HOST', '192.168.1.100')
mqttPort = os.environ.get('MQTT_PORT', '1883')
updateInterval = os.environ.get('MQQT_UPDATEINTERVAL','60')

httpProxy = os.environ.get('HTTP_PROXY', '')

if httpProxy:
    proxyDict = {
        "http": httpProxy
    }
else:
    proxyDict = None

def client_request_get(url, proxies, auth):
    try_count = 0
    scode = 0
    while scode != 200 and try_count < 10:
        try:
            try_count = try_count + 1
            r = requests.get(url, proxies=proxies, auth=auth)
            scode = r.status_code
            if scode != 200:
                logger.info("error for %s code %d" % (url, scode))
            time.sleep(1)
        except requests.exceptions.ChunkedEncodingError:
            time.sleep(1)
            pass
    return r

def on_connect(client, userdata, flags, rc):
    global rootTopic
    logger.info("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    logger.info("subscribe for %s/set/# to handle update requets to infowin" % (rootTopic))
    client.subscribe("%s/set/#" % (rootTopic))

def on_message(client, userdata, msg):
    logger.info(msg.topic+" "+str(msg.payload))
    #http://192.168.1.146/api/1.0/datapoint PUT
    #{OID: "/1/20/0/3/58/0", value: "0.0"}

    #print(configuredMQTTs)
    #print(msg.topic.encode('utf-8'))
    #print(msg.topic.encode('ascii', "replace"))

    #windhager/set/UML_HK1Kesseltemp-?berh?hung/Heizkreis/
    #windhager/set/UML_HK1Kesseltemp-?berh?hung/Heizkreis

    if msg.topic[-1] != '/':
        msg.topic = msg.topic + '/'

    mqttKey = getMqttKey(msg.topic)

    if mqttKey in configuredMQTTs:

        url = 'http://%s/api/1.0/datapoint' % (windhagerHost)
        logger.debug(url)
        logger.debug(msg.payload)
        j = json.loads(msg.payload)
        if 'value' in j:
            try:

                jsonPayload = '{"OID":"%s", "value":"%s"}' % (configuredMQTTs[mqttKey]['OID'], j['value'])
                r = requests.put(url, proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword), json=json.loads(jsonPayload))
                logger.debug(jsonPayload)
                logger.info(r.status_code)
                logger.debug(r.text)

            except:
                log.error(sys.exc_info()[0])

        else:
            logger.error('value not set in payload')

    else:

        url = 'http://%s/api/1.0/datapoint' % (windhagerHost)
        logger.debug(url)
        logger.debug(msg.payload)
        j = json.loads(msg.payload)
        if 'OID' in j and 'value' in j:
            r = requests.put(url, proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword), json=json.loads(msg.payload))
            logger.info(r.status_code)
            logger.debug(r.text)
        else:
            logger.error('payload (%s) not complete (OID and value is requested)' % msg.payload)




client = mqtt.Client()

client.on_message = on_message
client.on_connect = on_connect

client.connect(mgttHost, mqttPort, 60)

client.loop_start()

#print(configuredOIDs.keys())

#die

while True:

    for oid in configuredOIDs.keys():
        item = configuredOIDs[oid]
        #url = 'http://%s/api/1.0/lookup/1/%s' % (windhagerHost, oid[1:])
        #print(url)
        logger.debug(item['url'])
        scode = 0

        r = client_request_get(item['url'], proxies=proxyDict, auth=HTTPDigestAuth(mesUser, mesPassword))

        logger.debug (r.text)
        if r.status_code == 200:
            jc = json.loads(r.content)
            jc['apipath'] = item['mqttPath'].strip()
            #overwriting nvNames with values sent by the MES
            if 'nvName' in jc:
                item['name'] = jc['nvName']
            if 'writeProt' in jc:
                if not jc['writeProt']:
                    jc['updatepath']='%s/set/%s' % (rootTopic,jc['OID'])
                    try:
                        jc['updateobject']=json.loads('{"OID":"%s", "value":"%s"}' % (jc['OID'], jc['value']))
                    except KeyError:
                        jc['updateobject']=json.loads('{"OID":"%s", "value":""}' % (jc['OID']))

            if 'valuesList' in item:
                jc['valueList'] = item['valuesList']
                logger.debug(item)
                #try:
                if not type(item['valuesList']) is list:
                    jc['valueText'] = item['valuesList'][str(jc['value'])]
                #except TypeError:
                else:
                    jc['valueText'] = item['valuesList'][int(jc['value'])]

            else:
                try:
                    jc['valueText'] = str(jc['value'])
                except KeyError:
                    jc['valueText'] = ''

            if 'valueType' in item:
                jc['valueType'] = item['valueType']


            logger.info('toPublish Item %s/status/%s with value %s' % (rootTopic,item['mqttPath'], str(jc.get('value','undefined'))))
            item_published = False
            while not item_published:
                try:
                    mr=client.publish("%s/status/%s" % (rootTopic,item['mqttPath']), json.dumps(jc))
                    logger.debug({'is published':mr.is_published(), 'rc':mr.rc})
                    item_published = True
                except RuntimeError:
                    try_connect = True
                    try_count = 0
                    while try_connect:
                        try:
                            client.connect(mgttHost, mqttPort, 60)
                            try_connect = False
                        except socket.error:
                            try_count = try_count + 1
                            time.sleep(1)
                        if try_count > 20:
                            exit(1)
                            

    logger.info('publishing completed')
    time.sleep(int(updateInterval))

client.loop_stop(force=False)
